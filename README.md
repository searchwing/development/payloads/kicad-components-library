# kicad components library

KiCAD components library for searchwing PCB's. 

1. Add to Kicad-Git project with:

        git submodule add https://gitlab.com/searchwing/development/payloads/kicad-components-library.git


2. In KiCad, add ``searchwing_components.kicad_sym`` to project specific symbol library as well as ``searchwing_components.pretty`` to project specific footprint library.


